import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-mw-media-item',
  templateUrl: 'media-item.component.html',
  styleUrls: ['media-item.component.less']
})
export class MediaItemComponent {

  @Input('mediaItemAlias') mediaItemAlias;
  @Input() mediaItem;

  @Output() delete = new EventEmitter();

  onDelete() {
    console.log('deleted');
    this.delete.emit(this.mediaItem);
  }
}
