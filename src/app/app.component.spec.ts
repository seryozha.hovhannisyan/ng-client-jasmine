import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {AppComponent} from './app.component';
import {NoticeBoardComponent} from './karma/notice-board/notice-board.component';
import {StudentsComponent} from './karma/students/students.component';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './karma/components/home/home.component';
import {AboutComponent} from './karma/components/about/about.component';
import {QuoteTextComponent} from './karma/components/quote-text/quote-text.component';
import {APP_BASE_HREF} from '@angular/common';
import has = Reflect.has;
import {not} from 'rxjs/internal-compatibility';

describe('AppComponent', () => {
  const routes: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'about', component: AboutComponent},
    {path: '', redirectTo: '/home', pathMatch: 'full'}
  ];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        NoticeBoardComponent,
        StudentsComponent,
        HomeComponent,
        AboutComponent,
        QuoteTextComponent
      ],
      imports: [
        RouterTestingModule,
        RouterModule.forRoot(routes)
      ],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'}
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'NgClient'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('NgClient');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.description span').textContent).toContain('Keeping track of the media I want to watch.');
  });
  // karma
  it(`should have as title 'karma-project'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('NgClient');
  }));

  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Media Watch List NgClient');
  }));
  // https://jasmine.github.io/2.3/introduction
  describe('A suite', () => {
    it('contains spec with an expectation', () => {
      expect(true).toBe(true);
    });
  });

  describe('A suite is just a function', () => {
    let a;

    it('and so is a spec', () => {
      a = true;

      expect(a).toBe(true);
    });
  });

  describe('The \'toBe\' matcher compares with ===', () => {
    it('and has a positive case', () => {
      expect(true).toBe(true);
    });
    it('and can have a negative case', () => {
      expect(false).not.toBe(true);
    });
  });

  describe('Included matchers:', () => {

    it('The \'toBe\' matcher compares with ===', () => {
      const a = 12;
      const b = a;

      expect(a).toBe(b);
      expect(a).not.toBe(null);
    });

    describe('The \'toEqual\' matcher', () => {

      it('works for simple literals and variables', () => {
        const a = 12;
        expect(a).toEqual(12);
      });

      it('should work for objects', () => {
        const foo = {
          a: 12,
          b: 34
        };
        const bar = {
          a: 12,
          b: 34
        };
        expect(foo).toEqual(bar);
      });
    });

    it('The \'toMatch\' matcher is for regular expressions', () => {
      const message = 'foo bar baz';

      expect(message).toMatch(/bar/);
      expect(message).toMatch('bar');
      expect(message).not.toMatch(/quux/);
    });

    it('The \'toBeDefined\' matcher compares against `undefined`', () => {
      const a = {
        foo: 'foo'
      };

      expect(a.foo).toBeDefined();
      // expect(a.bar).not.toBeDefined(); //todo
    });

    it('The `toBeUndefined` matcher compares against `undefined`', () => {
      const a = {
        foo: 'foo'
      };

      expect(a.foo).not.toBeUndefined();
      // expect(a.bar).toBeUndefined(); //todo
    });

    it('The \'toBeNull\' matcher compares against null', () => {
      const a = null;
      const foo = 'foo';

      expect(null).toBeNull();
      expect(a).toBeNull();
      expect(foo).not.toBeNull();
    });

    it('The \'toBeTruthy\' matcher is for boolean casting testing', () => {
      // let a; //todo
      const foo = 'foo';

      expect(foo).toBeTruthy();
      // expect(a).not.toBeTruthy(); //todo
    });

    it('The \'toBeFalsy\' matcher is for boolean casting testing', () => {
      // let a; //todo
      const foo = 'foo';

      // expect(a).toBeFalsy(); //todo
      expect(foo).not.toBeFalsy();
    });

    describe('The \'toContain\' matcher', () => {
      it('works for finding an item in an Array', () => {
        const a = ['foo', 'bar', 'baz'];

        expect(a).toContain('bar');
        expect(a).not.toContain('quux');
      });

      it('also works for finding a substring', () => {
        const a = 'foo bar baz';

        expect(a).toContain('bar');
        expect(a).not.toContain('quux');
      });
    });

    it('The \'toBeLessThan\' matcher is for mathematical comparisons', () => {
      const pi = 3.1415926;
      const e = 2.78;

      expect(e).toBeLessThan(pi);
      expect(pi).not.toBeLessThan(e);
    });

    it('The \'toBeGreaterThan\' matcher is for mathematical comparisons', () => {
      const pi = 3.1415926;
      const e = 2.78;

      expect(pi).toBeGreaterThan(e);
      expect(e).not.toBeGreaterThan(pi);
    });

    it('The \'toBeCloseTo\' matcher is for precision math comparison', () => {
      const pi = 3.1415926;
      const e = 2.78;

      expect(pi).not.toBeCloseTo(e, 2);
      expect(pi).toBeCloseTo(e, 0);
    });

    it('The \'toThrow\' matcher is for testing if a function throws an exception', () => {
      const foo = () => {
        return 1 + 2;
      };
      const bar = () => {
        // return a + 1; //todo
      };
      const baz = () => {
        throw new Error('what');
      };

      expect(foo).not.toThrow();
      // expect(bar).toThrow(); //todo
      // expect(baz).toThrow('what'); //todo
    });

    it('The \'toThrowError\' matcher is for testing a specific thrown exception', () => {
      const foo = () => {
        throw new TypeError('foo bar baz');
      };

      expect(foo).toThrowError('foo bar baz');
      expect(foo).toThrowError(/bar/);
      expect(foo).toThrowError(TypeError);
      expect(foo).toThrowError(TypeError, 'foo bar baz');
    });
  });

  describe('A spec using the fail function', () => {
    // todo debug
    const foo = (x, callBack) => {
      if (x) {
        callBack();
      }
    };

    it('should not call the callBack', () => {
      foo(false, () => {
        fail('Callback has been called');
      });
    });
  });

  describe('A spec', () => {
    it('is just a function, so it can contain any code', () => {
      let foo = 0;
      foo += 1;

      expect(foo).toEqual(1);
    });

    it('can have more than one expectation', () => {
      let foo = 0;
      foo += 1;

      expect(foo).toEqual(1);
      expect(true).toEqual(true);
    });
  });

  describe('A spec using beforeAll and afterAll', () => {
    let foo;

    beforeAll(() => {
      foo = 1;
    });

    afterAll(() => {
      foo = 0;
    });

    it('sets the initial value of foo before specs run', () => {
      expect(foo).toEqual(1);
      foo += 1;
    });

    // it('does not reset foo between specs', () => {
    //   expect(foo).toEqual(2);
    // });
  });

  // describe('A spec', () => {
  //   beforeEach(() => {
  //     this.foo = 0;
  //   });
  //
  //   it('can use the `this` to share state', () => {
  //     expect(this.foo).toEqual(0);
  //     this.bar = 'test pollution?';
  //   });
  //
  //   it('prevents test pollution by having an empty `this` created for the next spec', () => {
  //     expect(this.foo).toEqual(0);
  //     expect(this.bar).toBe(undefined);
  //   });
  // });

  describe('A spec', () => {
    let foo;

    beforeEach(() => {
      foo = 0;
      foo += 1;
    });

    afterEach(() => {
      foo = 0;
    });

    it('is just a function, so it can contain any code', () => {
      expect(foo).toEqual(1);
    });

    it('can have more than one expectation', () => {
      expect(foo).toEqual(1);
      expect(true).toEqual(true);
    });

    describe('nested inside a second describe', () => {
      let bar;

      beforeEach(() => {
        bar = 1;
      });

      it('can reference both scopes as needed', () => {
        expect(foo).toEqual(bar);
      });
    });
  });

  xdescribe('A spec', () => {
    let foo;

    beforeEach(() => {
      foo = 0;
      foo += 1;
    });

    it('is just a function, so it can contain any code', () => {
      expect(foo).toEqual(1);
    });
  });

  describe('Pending specs', () => {

    xit('can be declared \'xit\'', () => {
      expect(true).toBe(false);
    });

    it('can be declared with \'it\' but without a function');

    it('can be declared by calling \'pending\' in the spec body', () => {
      expect(true).toBe(false);
      pending('this is why it is pending');
    });
  });

  describe('A spy', () => {
    let foo;
    let bar = null;

    beforeEach(() => {
      foo = {
        setBar(value) {
          bar = value;
        }
      };

      spyOn(foo, 'setBar');

      foo.setBar(123);
      foo.setBar(456, 'another param');
    });

    it('tracks that the spy was called', () => {
      expect(foo.setBar).toHaveBeenCalled();
    });

    it('tracks all the arguments of its calls', () => {
      expect(foo.setBar).toHaveBeenCalledWith(123);
      expect(foo.setBar).toHaveBeenCalledWith(456, 'another param');
    });

    it('stops all execution on a function', () => {
      expect(bar).toBeNull();
    });
  });

  describe('A spy, when configured to call through', () => {
    let foo;
    let bar;
    let fetchedBar;

    beforeEach(() => {
      foo = {
        setBar(value) {
          bar = value;
        },
        getBar: () => {
          return bar;
        }
      };

      spyOn(foo, 'getBar').and.callThrough();

      foo.setBar(123);
      fetchedBar = foo.getBar();
    });

    it('tracks that the spy was called', () => {
      expect(foo.getBar).toHaveBeenCalled();
    });

    it('should not affect other functions', () => {
      expect(bar).toEqual(123);
    });

    it('when called returns the requested value', () => {
      expect(fetchedBar).toEqual(123);
    });
  });

  describe('A spy, when configured to fake a return value', () => {
    let foo;
    let bar;
    let fetchedBar;

    beforeEach(() => {
      foo = {
        setBar(value) {
          bar = value;
        },
        getBar: () => {
          return bar;
        }
      };

      spyOn(foo, 'getBar').and.returnValue(745);

      foo.setBar(123);
      fetchedBar = foo.getBar();
    });

    it('tracks that the spy was called', () => {
      expect(foo.getBar).toHaveBeenCalled();
    });

    it('should not affect other functions', () => {
      expect(bar).toEqual(123);
    });

    it('when called returns the requested value', () => {
      expect(fetchedBar).toEqual(745);
    });
  });

  describe('A spy, when configured with an alternate implementation', () => {
    let foo;
    let bar;
    let fetchedBar;

    beforeEach(() => {
      foo = {
        setBar(value) {
          bar = value;
        },
        getBar: () => {
          return bar;
        }
      };

      spyOn(foo, 'getBar').and.callFake(() => {
        return 1001;
      });

      // spyOn(foo, 'getBar').and.callFake((arguments, can, be, received) => {
      //   return 1001;
      // }); // todo

      foo.setBar(123);
      fetchedBar = foo.getBar();
    });

    it('tracks that the spy was called', () => {
      expect(foo.getBar).toHaveBeenCalled();
    });

    it('should not affect other functions', () => {
      expect(bar).toEqual(123);
    });

    it('when called returns the requested value', () => {
      expect(fetchedBar).toEqual(1001);
    });
  });

  describe('A spy, when configured to throw an error', () => {
    let foo;
    let bar;

    beforeEach(() => {
      foo = {
        setBar(value) {
          bar = value;
        }
      };

      spyOn(foo, 'setBar').and.throwError('quux');
    });

    it('throws the value', () => {
      expect(() => {
        foo.setBar(123);
      }).toThrowError('quux');
    });
  });

  // Spies: and.stub
  // When a calling strategy is used for a spy, the original stubbing behavior can be returned at any time with and.stub.
  describe('A spy', () => {
    let foo;
    let bar = null;

    beforeEach(() => {
      foo = {
        setBar(value) {
          bar = value;
        }
      };

      spyOn(foo, 'setBar').and.callThrough();
    });

    it('can call through and then stub in the same spec', () => {
      foo.setBar(123);
      expect(bar).toEqual(123);

      foo.setBar.and.stub();
      bar = null;

      foo.setBar(123);
      expect(bar).toBe(null);
    });
  });

  describe('A spy', () => {
    let foo;
    let bar = null;

    beforeEach(() => {
      foo = {
        setBar(value) {
          bar = value;
        }
      };

      spyOn(foo, 'setBar');
    });

    // .calls.any(): returns false if the spy has not been called at all, and then true once at least one call happens.
    it('tracks if it was called at all', () => {
      expect(foo.setBar.calls.any()).toEqual(false);

      foo.setBar();

      expect(foo.setBar.calls.any()).toEqual(true);
    });

    // .calls.count(): returns the number of times the spy was called
    it('tracks the number of times it was called', () => {
      expect(foo.setBar.calls.count()).toEqual(0);

      foo.setBar();
      foo.setBar();

      expect(foo.setBar.calls.count()).toEqual(2);
    });

    // .calls.argsFor(index): returns the arguments passed to call number index
    it('tracks the arguments of each call', () => {
      foo.setBar(123);
      foo.setBar(456, 'baz');

      expect(foo.setBar.calls.argsFor(0)).toEqual([123]);
      expect(foo.setBar.calls.argsFor(1)).toEqual([456, 'baz']);
    });

    // .calls.allArgs(): returns the arguments to all calls
    it('tracks the arguments of all calls', () => {
      foo.setBar(123);
      foo.setBar(456, 'baz');

      expect(foo.setBar.calls.allArgs()).toEqual([[123], [456, 'baz']]);
    });

    // .calls.all(): returns the context (the this) and arguments passed all calls
    it('can provide the context and arguments to all calls', () => {
      foo.setBar(123);
      // todo open bellow
      // expect(foo.setBar.calls.all()).toEqual([{object: foo, args: [123], returnValue: undefined}]);
    });

    // .calls.mostRecent(): returns the context (the this) and arguments for the most recent call
    it('has a shortcut to the most recent call', () => {
      foo.setBar(123);
      foo.setBar(456, 'baz');
      // todo open bellow
      // expect(foo.setBar.calls.mostRecent()).toEqual({object: foo, args: [456, 'baz'], returnValue: undefined});
    });

    // .calls.first(): returns the context (the this) and arguments for the first call
    it('has a shortcut to the first call', () => {
      foo.setBar(123);
      foo.setBar(456, 'baz');
      // todo open bellow
      // expect(foo.setBar.calls.first()).toEqual({object: foo, args: [123], returnValue: undefined});
    });

    // When inspecting the return from all(), mostRecent() and first(),
    // the object property is set to the value of this when the spy was called.
    it('tracks the context', () => {
      const spy = jasmine.createSpy('spy');
      const baz = {
        fn: spy
      };
      const quux = {
        fn: spy
      };
      baz.fn(123);
      quux.fn(456);

      expect(spy.calls.first().object).toBe(baz);
      expect(spy.calls.mostRecent().object).toBe(quux);
    });

    // .calls.reset(): clears all tracking for a spy
    it('can be reset', () => {
      foo.setBar(123);
      foo.setBar(456, 'baz');

      expect(foo.setBar.calls.any()).toBe(true);

      foo.setBar.calls.reset();

      expect(foo.setBar.calls.any()).toBe(false);
    });
  });

  describe('A spy, when created manually', () => {
    let whatAmI;

    beforeEach(() => {
      whatAmI = jasmine.createSpy('whatAmI');

      whatAmI('I', 'am', 'a', 'spy');
    });

    // it('is named, which helps in error reporting', () => {
    //   expect(whatAmI.and.identity()).toEqual('whatAmI');
    // });

    it('tracks that the spy was called', () => {
      expect(whatAmI).toHaveBeenCalled();
    });

    it('tracks its number of calls', () => {
      expect(whatAmI.calls.count()).toEqual(1);
    });

    it('tracks all the arguments of its calls', () => {
      expect(whatAmI).toHaveBeenCalledWith('I', 'am', 'a', 'spy');
    });

    it('allows access to the most recent call', () => {
      expect(whatAmI.calls.mostRecent().args[0]).toEqual('I');
    });
  });

  // Spies: createSpyObj
  // In order to create a mock with multiple spies, use jasmine.createSpyObj and pass an array of strings.
  // It returns an object that has a property for each string that is a spy.
  describe('Multiple spies, when created manually', () => {
    let tape;

    beforeEach(() => {
      tape = jasmine.createSpyObj('tape', ['play', 'pause', 'stop', 'rewind']);

      tape.play();
      tape.pause();
      tape.rewind(0);
    });

    it('creates spies for each requested function', () => {
      expect(tape.play).toBeDefined();
      expect(tape.pause).toBeDefined();
      expect(tape.stop).toBeDefined();
      expect(tape.rewind).toBeDefined();
    });

    it('tracks that the spies were called', () => {
      expect(tape.play).toHaveBeenCalled();
      expect(tape.pause).toHaveBeenCalled();
      expect(tape.rewind).toHaveBeenCalled();
      expect(tape.stop).not.toHaveBeenCalled();
    });

    it('tracks all the arguments of its calls', () => {
      expect(tape.rewind).toHaveBeenCalledWith(0);
    });
  });

  // Matching Anything with jasmine.any
  // jasmine.any takes a constructor or "class" name as an expected value.
  // It returns true if the constructor matches the constructor of the actual value.
  describe('jasmine.any', () => {
    it('matches any value', () => {
      expect({}).toEqual(jasmine.any(Object));
      expect(12).toEqual(jasmine.any(Number));
    });

    describe('when used with a spy', () => {
      it('is useful for comparing arguments', () => {
        const foo = jasmine.createSpy('foo');
        foo(12, () => {
          return true;
        });

        expect(foo).toHaveBeenCalledWith(jasmine.any(Number), jasmine.any(Function));
      });
    });
  });

  // Matching existence with jasmine.anything
  // jasmine.anything returns true if the actual value is not null or undefined.
  describe('jasmine.anything', () => {
    it('matches anything', () => {
      expect(1).toEqual(jasmine.anything());
    });

    describe('when used with a spy', () => {
      it('is useful when the argument can be ignored', () => {
        const foo = jasmine.createSpy('foo');
        foo(12, () => {
          return false;
        });

        expect(foo).toHaveBeenCalledWith(12, jasmine.anything());
      });
    });
  });

  // Partial Matching with jasmine.objectContaining
  // jasmine.objectContaining is for those times when an expectation only cares about certain key/value pairs in the actual.
  describe('jasmine.objectContaining', () => {
    let foo;

    beforeEach(() => {
      foo = {
        a: 1,
        b: 2,
        bar: 'baz'
      };
    });

    it('matches objects with the expect key/value pairs', () => {
      expect(foo).toEqual(jasmine.objectContaining({
        bar: 'baz'
      }));
      expect(foo).not.toEqual(jasmine.objectContaining({
        c: 37
      }));
    });

    describe('when used with a spy', () => {
      it('is useful for comparing arguments', () => {
        const callback = jasmine.createSpy('callback');

        callback({
          bar: 'baz'
        });

        expect(callback).toHaveBeenCalledWith(jasmine.objectContaining({
          bar: 'baz'
        }));
        expect(callback).not.toHaveBeenCalledWith(jasmine.objectContaining({
          c: 37
        }));
      });
    });
  });

  // Partial Array Matching with jasmine.arrayContaining
  // jasmine.arrayContaining is for those times when an expectation only cares about some of the values in an array.
  describe('jasmine.arrayContaining', () => {
    let foo;

    beforeEach(() => {
      foo = [1, 2, 3, 4];
    });

    it('matches arrays with some of the values', () => {
      expect(foo).toEqual(jasmine.arrayContaining([3, 1]));
      expect(foo).not.toEqual(jasmine.arrayContaining([6]));
    });

    describe('when used with a spy', () => {
      it('is useful when comparing arguments', () => {
        const callback = jasmine.createSpy('callback');

        callback([1, 2, 3, 4]);

        expect(callback).toHaveBeenCalledWith(jasmine.arrayContaining([4, 2, 3]));
        expect(callback).not.toHaveBeenCalledWith(jasmine.arrayContaining([5, 2]));
      });
    });
  });

  // String Matching with jasmine.stringMatching
  // jasmine.stringMatching is for when you don't want to match a string in a larger object exactly,
  // or match a portion of a string in a spy expectation.
  describe('jasmine.stringMatching', () => {
    it('matches as a regexp', () => {
      expect({foo: 'bar'}).toEqual({foo: jasmine.stringMatching(/^bar$/)});
      expect({foo: 'foobarbaz'}).toEqual({foo: jasmine.stringMatching('bar')});
    });

    describe('when used with a spy', () => {
      it('is useful for comparing arguments', () => {
        const callback = jasmine.createSpy('callback');

        callback('foobarbaz');

        expect(callback).toHaveBeenCalledWith(jasmine.stringMatching('bar'));
        expect(callback).not.toHaveBeenCalledWith(jasmine.stringMatching(/^bar$/));
      });
    });
  });

  // Custom asymmetric equality tester
  // When you need to check that something meets a certain criteria, without being strictly equal,
  // you can also specify a custom asymmetric equality tester simply by providing an object that has an asymmetricMatch function.
  describe('custom asymmetry', () => {
    const tester = {
      asymmetricMatch(actual) {
        const secondValue = actual.split(',')[1];
        return secondValue === 'bar';
      }
    };

    it('dives in deep', () => {
      expect('foo,bar,baz,quux').toEqual(tester);
    });

    describe('when used with a spy', () => {
      it('is useful for comparing arguments', () => {
        const callback = jasmine.createSpy('callback');

        callback('foo,bar,baz');

        expect(callback).toHaveBeenCalledWith(tester);
      });
    });
  });

  // Jasmine Clock
  // This syntax has changed for Jasmine 2.0. The Jasmine Clock is available for testing time dependent code.
  describe('Manually ticking the Jasmine Clock', () => {
    let timerCallback;

    // It is installed with a call to jasmine.clock().install in a spec or suite that needs to manipulate time.
    beforeEach(() => {
      timerCallback = jasmine.createSpy('timerCallback');
      jasmine.clock().install();
    });

    // Be sure to uninstall the clock after you are done to restore the original functions.
    afterEach(() => {
      jasmine.clock().uninstall();
    });

    // Mocking the JavaScript Timeout Functions
    // You can make setTimeout or setInterval synchronous executing the registered functions only once the clock is ticked forward in time.
    // To execute registered functions, move time forward via the jasmine.clock().tick function, which takes a number of milliseconds.
    it('causes a timeout to be called synchronously', () => {
      setTimeout(() => {
        timerCallback();
      }, 100);

      expect(timerCallback).not.toHaveBeenCalled();

      jasmine.clock().tick(101);

      expect(timerCallback).toHaveBeenCalled();
    });

    it('causes an interval to be called synchronously', () => {
      setInterval(() => {
        timerCallback();
      }, 100);

      expect(timerCallback).not.toHaveBeenCalled();

      jasmine.clock().tick(101);
      expect(timerCallback.calls.count()).toEqual(1);

      jasmine.clock().tick(50);
      expect(timerCallback.calls.count()).toEqual(1);

      jasmine.clock().tick(50);
      expect(timerCallback.calls.count()).toEqual(2);
    });

    // Mocking the Date
    // The Jasmine Clock can also be used to mock the current date.
    describe('Mocking the Date object', () => {
      it('mocks the Date object and sets it to a given time', () => {
        const baseTime = new Date(2013, 9, 23);

        // If you do not provide a base time to mockDate it will use the current date.
        jasmine.clock().mockDate(baseTime);

        jasmine.clock().tick(50);
        expect(new Date().getTime()).toEqual(baseTime.getTime() + 50);
      });
    });
  });

  // Asynchronous Support
  // This syntax has changed for Jasmine 2.0. Jasmine also has support for running specs that require testing asynchronous operations.
  describe('Asynchronous specs', () => {
    let value;

    // Calls to beforeAll, afterAll, beforeEach, afterEach,
    // and it can take an optional single argument that should be called when the async work is complete.
    beforeEach((done) => {
      setTimeout(() => {
        value = 0;
        done();
      }, 1);
    });

    // This spec will not start until the done function is called in the call to beforeEach above.
    // And this spec will not complete until its done is called.
    it('should support async execution of test preparation and expectations', (done) => {
      value++;
      expect(value).toBeGreaterThan(0);
      done();
    });

    // By default jasmine will wait for 5 seconds for an asynchronous spec to finish before causing a timeout failure.
    // If the timeout expires before done is called, the current spec will be marked as failed
    // and suite execution will continue as if done was called.
    // If specific specs should fail faster or need more time this can be adjusted by passing a timeout value to it, etc.
    // If the entire suite should have a different timeout,
    // jasmine.DEFAULT_TIMEOUT_INTERVAL can be set globally, outside of any given describe.
    describe('long asynchronous specs', () => {
      beforeEach((done) => {
        done();
      }, 1000);

      it('takes a long time', (done) => {
        setTimeout(() => {
          done();
        }, 9000);
      }, 10000);

      afterEach((done) => {
        done();
      }, 1000);
    });

    // The done.fail function fails the spec and indicates that it has completed.
    describe('A spec using done.fail', () => {
      const foo = (x, callBack1, callBack2) => {
        if (x) {
          setTimeout(callBack1, 0);
        } else {
          setTimeout(callBack2, 0);
        }
      };

      it('should not call the second callBack', (done) => {
        foo(true,
          done,
          () => {
            done.fail('Second callback has been called');
          }
        );
      });
    });
  });

});
