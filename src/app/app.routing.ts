import {Routes, RouterModule} from '@angular/router';
import {MediaItemFormComponent} from './form-component/media-item-form.component';
import {MediaItemModelFormComponent} from './form-model-driven/media-item-form.component';
import {MediaItemListComponent} from './media-list/media-item-list.component';
import {NgbdModalBasic} from './bootstrap/modal/modal-basic';
import {NgbdTableComplete} from './bootstrap/table/table-complete';
import {HomeComponent} from './karma/components/home/home.component';
import {AboutComponent} from './karma/components/about/about.component';
import {ContactComponent} from './karma/components/contact/contact.component';
import {UserComponent} from './karma/components/user/user.component';

const appRoutes: Routes = [
  // karma rout
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'users', component: UserComponent },
  // { path: '', redirectTo: '/home', pathMatch: 'full'}
  {path: 'add', component: MediaItemModelFormComponent},
  {path: 'bootstrap/modal', component: NgbdModalBasic},
  {path: 'bootstrap/table', component: NgbdTableComplete},
  {path: ':medium', component: MediaItemListComponent},
  // todo ask to Hro
  // {path: '', pathMatch: 'full', redirectTo: 'kuku' }
];

export const routing = RouterModule.forRoot(appRoutes);
