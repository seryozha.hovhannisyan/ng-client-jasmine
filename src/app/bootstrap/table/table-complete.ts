import {DecimalPipe} from '@angular/common';
import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {Observable} from 'rxjs';

import {COUNTRIES} from './countries';
import {Country} from './country';
import {CountryService} from './country.service';
import {CountryHttpService} from './country-http-service';
import {NgbdSortableHeader, SortEvent} from './sortable.directive';


@Component({
  selector: 'ngbd-table-complete',
  templateUrl: './table-complete.html',
  providers: [CountryService, DecimalPipe],
  styleUrls: ["table-complete.less"]
})
export class NgbdTableComplete implements OnInit{
  countries$: Observable<Country[]>;
  total$: Observable<number>;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(
    private countryHttpService: CountryHttpService,
    public service: CountryService
  ) {
    service.countries = COUNTRIES;
    this.countries$ = service.countries$;
    this.total$ = service.total$;
  }

  ngOnInit() {

  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    console.log('column', column)
    console.log('direction', direction)

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  add (){
    const country = {}
    this.countryHttpService.add(country).subscribe(
      countries => {
        console.log('countries', countries);
        COUNTRIES.push({
          id: 99,
          name: 'aaaaa',
          flag: 'f/f3/Flag_of_Russia.svg',
          area: 1,
          population: 1
        });
        return this.service.countries = COUNTRIES;
      }
    );

    this.service._search();

  }

  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    // this.pageOfItems = pageOfItems;
    console.log('pageOfItems', pageOfItems)
  }
  private onPageChange = (pageNumber) => {
    // this.pageChange.emit(pageNumber)
    console.log('pageNumber', pageNumber)
  }

  getPageSymbol(current: number) {
    // console.log('current.', current)
    return ['A', 'B', 'C', 'D', 'E', 'F', 'G'][current - 1];
  }
}
