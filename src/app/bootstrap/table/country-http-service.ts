import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import {Country} from './country';

@Injectable()
export class CountryHttpService {
  constructor(private httpClient: HttpClient) {}
  get(name: string) {
    const options = {
      params: {
        name
      }
    };
    return this.httpClient.get<CountryResponse>('http://localhost:9000/api/countries', options).pipe(
      map(
        (response: CountryResponse) => {
          return response.countries;
        }
      )
    );
  }
  add(country) {
    return this.httpClient.get<CountryResponse>('http://localhost:9000/api/mediaItems').pipe(
      map(
        (response: CountryResponse) => {
          console.log('response', response)
          return {
            id: 100,
            name: 'aaaaa',
            flag: 'f/f3/Flag_of_Russia.svg',
            area: 0,
            population: 0
          };
        }
      )
    );
  }
  delete(country) {
    return this.httpClient.delete('http://localhost:9000/api/country/${country.id}');
  }
}
interface CountryResponse {
  // todo ask to Hro
  countries: Country[];
}
