import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { throwError } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import {NgbdModalBasic} from './modal-basic';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgbDate, NgbModule } from '@ng-bootstrap/ng-bootstrap';


describe('NgbdModalBasic', () => {
  let component: NgbdModalBasic;
  let fixture: ComponentFixture<NgbdModalBasic>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, NgbModule],
      declarations: [NgbdModalBasic],
      // providers: [{ provide: StudentsService, useClass: StudentsServiceStub }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgbdModalBasic);
    component = fixture.componentInstance;
    // service = TestBed.get(StudentsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  //
  // it('should call getDetails() with proper id when a value is selected from HTML', () => {
  //
  //   let modal = fixture.debugElement.query(By.directive('ngbDatepicker'));
  //   expect(modal.nativeElement.style.visibility).toBe('false');
  //   //
  //   const ele = fixture.debugElement.nativeElement.querySelector('#modal_button');
  //   ele.click();
  //   fixture.detectChanges();
  //   //
  //   modal = fixture.debugElement.query(By.css('modal'));
  //   expect(modal.nativeElement.style.visibility).toBe('false');
  //   // expect(fixture.debugElement.nativeElement.querySelector('body')).toHaveClass('modal-open');
  // });
  //
  // it('should set Error message when getUserDetails() is errored out', () => {
  //   expect(component.err_msg).toBeUndefined();
  //   spyOn(component._userService, 'getUserDetails').and.returnValue(throwError('Error'));
  //   const ele = fixture.debugElement.nativeElement.querySelector('#usr-1');
  //   ele.click();
  //   expect(component.err_msg).toBe('Error while loading User Details');
  // });
});
