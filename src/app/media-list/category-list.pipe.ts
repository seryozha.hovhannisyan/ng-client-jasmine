import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name : 'categoryList',
  pure : false
})
export class CategoryListPipe implements PipeTransform{
  transform(mediaItems, arg1, arg2, arg3) {
    const categories = [];
    console.log(arg1 +  arg2 + arg3, mediaItems);
    if (mediaItems) {
      mediaItems.forEach(mediaItem => {
        if (categories.indexOf(mediaItem.category) <= -1) {
          categories.push(mediaItem.category);
        }
      });
    }
    return categories.join(', ');
  }
}
