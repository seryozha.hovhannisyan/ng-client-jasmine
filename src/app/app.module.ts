import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpXhrBackend } from '@angular/common/http';

import { AppComponent } from './app.component';
import { MediaItemComponent } from './media/media-item.component';
import { MediaItemListComponent } from './media-list/media-item-list.component';
import { CategoryListPipe } from './media-list/category-list.pipe';
import { MediaItemFormComponent } from './form-component/media-item-form.component';
import { MediaItemModelFormComponent } from './form-model-driven/media-item-form.component';
// bootstrap start
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import {NgbdModalBasic} from './bootstrap/modal/modal-basic';
import {NgbdRatingForm} from './bootstrap/rating-form/rating-form';
import { NgbdSortableHeader } from './bootstrap/table/sortable.directive';
import { NgbdTableComplete } from './bootstrap/table/table-complete';

import { CountryHttpService } from './bootstrap/table/country-http-service';
// bootstrap end
import { MediaItemService } from './service/media-item-service';

import { FavoriteDirective } from './media/favorite.directve';
import {providerDef} from '@angular/compiler/src/view_compiler/provider_compiler';
import {lookupListToken, lookupLists} from './providers';
// import {MockXHRBackend} from './mock-xhr-backend';
import {routing} from './app.routing';


import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
// karma
import {NoticeBoardComponent} from './karma/notice-board/notice-board.component';
import {StudentsComponent} from './karma/students/students.component';
import {StudentsService} from './karma/students/students.service';
import {HomeComponent} from './karma/components/home/home.component';
import {AboutComponent} from './karma/components/about/about.component';
import {ContactComponent} from './karma/components/contact/contact.component';
import {QuoteTextComponent} from './karma/components/quote-text/quote-text.component';
import {UserComponent} from './karma/components/user/user.component';
import {RouterModule, Routes} from '@angular/router';
import {UserService} from './karma/components/user/user.service';

registerLocaleData(localeFr, 'fr-FR', localeFrExtra);

// const lookupLists = {mediums : ['Movies', 'Series', 'Films']};


@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    routing
  ],
  declarations: [
    AppComponent,
    MediaItemComponent,
    MediaItemListComponent,
    FavoriteDirective,
    CategoryListPipe,
    MediaItemFormComponent,
    MediaItemModelFormComponent,
    NgbdModalBasic,
    NgbdRatingForm,
    NgbdSortableHeader,
    NgbdTableComplete,
    NoticeBoardComponent, StudentsComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    QuoteTextComponent,
    UserComponent
  ],
  // exports: [NgbdModalBasic],
  providers: [
    CountryHttpService,
    MediaItemService,
    StudentsService,
    {provide : lookupListToken, useValue: lookupLists},
    // {provide : MockXHRBackend, useValue: MockXHRBackend}
    UserService
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule {}
